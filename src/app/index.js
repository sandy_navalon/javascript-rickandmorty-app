import './styles/styles.scss';
import 'bootstrap';
import {displayRickList, displayMortyList} from './view/view-list';
import {displayRandomCharacter} from './view/view-random';
import {displayCharacterDetail } from "./view/view-details";

/*
import {RickAndMortyUtil} from "./utils/rickandmorty-utils" */

function addListeners() {
  document.getElementById('rick-characters').addEventListener('click', displayRickList);
  document.getElementById('morty-characters').addEventListener('click', displayMortyList);
  document.getElementById('random-characters').addEventListener('click', displayRandomCharacter);
  document.getElementById('character-finder').addEventListener('click', displayCharacterDetail);
 /*  document.querySelector('.body').onscroll = handleScroll; */
  /* console.log('addListeners'); */
}

window.onload = function () {
  addListeners();
};
