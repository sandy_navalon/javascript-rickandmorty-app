class RickAndMortyClass{
    constructor( id, name, image, type, status, origin){
        this.id = id;
        this.name = name;
        this.image = image;
        this.type = type;
        this.status = status;
        this.origin = origin;
    }

    getRickAndMortyId(){
        return this.id;
    }

    getRickAndMortyName(){
        return this.name;
    }

    getRickAndMortyImg(){
        return this.image;
    }

    getRickAndMortyType(){
        return this.type;
    }

    getRickAndMortyStatus(){
        return this.status;

    }

    getRickAndMortyOrigin(){
        return this.origin.name;
    }

}

class CharacterDetailClass extends RickAndMortyClass {
    constructor(id, name, image, type, status, origin, species) {
        super(id, name, image, type, status, origin);
        this.species = species;
    }

    getRickAndMortySpecies(){
        return this.species;
    }

}
class CharacterRandomizer extends RickAndMortyClass{
    constructor(id, name, image, type, status, origin, location) {
        super( id, name, image, type, status, origin);
        this.location = location;
    }

    getRickAndMortyLocation(){
        return this.location.name;
    }

}

export {RickAndMortyClass, CharacterRandomizer, CharacterDetailClass};