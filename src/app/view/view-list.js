import { fetchRickList } from "../api/api-list";
import { fetchMortyList } from "../api/api-list";
/* import { fetchPageCharacter } from "../api/api-list"; */

const displayMortyList = () => {
	const list = document.getElementById('characterList');
	list.innerHTML = ''; // clean content

	fetchMortyList().then((mortyList) => {
		mortyList.forEach(element => {

			const li = document.createElement('li');
			li.classList.add('item');

			const image = document.createElement('img');
			image.setAttribute('class','item-image');
			image.src = element.image;
			li.appendChild(image);

			const h3 = document.createElement('h3');
			h3.setAttribute('class', 'item-title');
			h3.innerText = element.name;
			li.appendChild(h3);

			const p = document.createElement('p');
			p.innerHTML = `Status: ${element.status}`;
			li.appendChild(p);

			const p2 = document.createElement('p');
			p2.innerHTML = `Last location: ${element.location.name}`;
			li.appendChild(p2);

			list.appendChild(li);

		});

	})

}

	//const newDivMorty = document.createElement('div');
		/* newDivMorty.innerHTML = ''; */
	//	newDivMorty.setAttribute('class', 'btn-div');

	//const prevBtnMorty = document.createElement('button');
	/* prevBtnMorty.id ='prev-btn-morty'; */
	//prevBtnMorty.textContent = 'Previous';
	//LISTENER

	//newDiv.appendChild(prevBtnMorty);

	//const nextBtnMorty = document.createElement('button');
	/* nextBtnMorty.id ='next-btn-morty'; */
	//nextBtnMorty.textContent = 'Next';
	//LISTENER
	//newDiv.appendChild(nextBtnMorty);

	//list.appendChild(newDivMorty);



/*const charactersList = ['Rick Sanchez', 'Morty Smith', 'Summer Smith', 'Mr. Poopybutthole'] */


const displayRickList = () => {
	const list = document.getElementById('characterList');
	list.innerHTML = ''; // clean content

	fetchRickList().then((rickList) => {
		rickList.forEach(element => {

			const li = document.createElement('li');
			li.classList.add('item');

			const image = document.createElement('img');
			image.setAttribute('class','item-image');
			image.src = element.image;
			li.appendChild(image);

			const h3 = document.createElement('h3');
			h3.setAttribute('class', 'item-title');
			h3.innerText = element.name;
			li.appendChild(h3);

			const p = document.createElement('p');
			p.innerHTML = `Status: ${element.status}`;
			li.appendChild(p);

			const p2 = document.createElement('p');
			p2.innerHTML = `Last location: ${element.location.name}`;
			li.appendChild(p2);

			list.appendChild(li);
		});

	})

}

export {displayRickList, displayMortyList};