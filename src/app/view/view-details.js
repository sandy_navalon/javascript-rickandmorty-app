import { fetchDetailList } from '../api/api-detail';
import { CharacterDetailClass } from "../models/rickandmortyclass";

const displayCharacterDetail = async () => {
  const characterList = document.getElementById('characterList');
  const characterName = document.getElementById('character-name').value;

  const character = await fetchDetailList(characterName);
  const characterDetail = new CharacterDetailClass(
    character.id,
    character.name,
    character.image,
    character.type,
    character.status,
    character.origin,
    character.species,
  );

  const characterHTMLString =

  `<li class="item-details">
      <img class="item-image" src="${ characterDetail.getRickAndMortyImg() }"/>
      <h2 class="item-title">${characterDetail.getRickAndMortyName() }</h2>
      <h3 class="item-title2">ID ${characterDetail.getRickAndMortyId() }</h3>
      <p class="item-subtitle">SPECIE: ${ characterDetail.getRickAndMortySpecies() }</p>
      <p class="item-subtitle" >STATUS: ${ characterDetail.getRickAndMortyStatus() }</p>
      <p class="item-subtitle">FROM... ${ characterDetail.getRickAndMortyOrigin() }</p>
  </li>`;

  characterList.innerHTML = characterHTMLString;
};

export { displayCharacterDetail }