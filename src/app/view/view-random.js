import { fetchDetailList } from '../api/api-detail';
import {CharacterRandomizer} from "../models/rickandmortyclass";


const displayRandomCharacter = async() => {
    const randomList = document.getElementById('characterList');
	randomList.innerHTML = ''; // clean content
    const characterId = Math.round(Math.random()*(826 - 1) + 1);

    const pickedCharacter = await fetchDetailList(characterId);
	const characterClass = new CharacterRandomizer(
		pickedCharacter.id,
		pickedCharacter.name,
		pickedCharacter.image,
		pickedCharacter.type,
		pickedCharacter.status,
		pickedCharacter.origin,
		pickedCharacter.location,
		 );

    const characterHTMLString =
    `<li class="item">
        <img class="item-image" src="${ characterClass.getRickAndMortyImg() }"/>
        <h2 class="item-title">${characterClass.getRickAndMortyName() }</h2>
		<h4 class="item-title2">ID ${characterClass.getRickAndMortyId() }</h4>
        <p class="item-subtitle">STATUS: ${ characterClass.getRickAndMortyStatus() }</p>
		<p class="item-subtitle">LAST SEEN IN... ${ characterClass.getRickAndMortyLocation() }</p>
    </li>`;

    randomList.innerHTML = characterHTMLString;

}

export {displayRandomCharacter}