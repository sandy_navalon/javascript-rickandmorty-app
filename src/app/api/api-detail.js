
/* import { RickAndMortyUtil } from "../utils/rickandmorty-utils"; */

const fetchDetailList = async (nameCharacter) => {

    /* RickAndMortyUtil.toggleLoader(); */
    const res = await fetch (`https://rickandmortyapi.com/api/character/${nameCharacter}`);


    const dataResponse = await res.json();

        let character = {
            id: dataResponse.id,
            name: dataResponse.name,
            image: dataResponse.image,
            type: dataResponse.type,
            status: dataResponse.status,
            origin: dataResponse.origin,
            species: dataResponse.species,
            location: dataResponse.location,

            /* console.log(character.name); */
        }

    /* RickAndMortyUtil.toggleLoader(); */

    return character;
}

export {fetchDetailList};