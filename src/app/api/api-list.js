
const fetchMortyList = async () => {
    const url = 'https://rickandmortyapi.com/api/character?name=morty';
    const promise = await fetch (url);

    const response = await promise.json();
    /* console.log(response.results); */
    return response.results;
}

const fetchRickList = async () => {
    const url = 'https://rickandmortyapi.com/api/character?name=rick';
    const promise = await fetch (url);

    const response = await promise.json();
    /* console.log(response.results); */
    return response.results;
}

/* const fetchPageCharacter = async(url)=>{

    const promise = await fetch(url);
    const {results:object} = promise.json();//destruncturing

    const getIdFromUrl = (url)=>url.split('/').slice('-2').join('');
    const characterList = data.map(({name, url}) => ({name, id: getIdFromUrl(url)}));

    return characterList;

} */

export {fetchMortyList, fetchRickList}